package com.rabo.customer.DTO;

import com.rabo.customer.common.BaseAddress;

/**
 * 
 * This is the DTO class for mapping customer's address request data.
 *
 */
public class AddressDTO extends BaseAddress {
	
}
