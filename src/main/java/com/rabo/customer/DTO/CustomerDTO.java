package com.rabo.customer.DTO;

import java.time.LocalDate;

import javax.validation.Valid;

/**
 * 
 * This is the DTO class for data converter
 *
 */
public class CustomerDTO {
	
	private String customerId;
	private String firstName;
	private String lastName;
	private LocalDate age;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getAge() {
		return age;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	public AddressDTO getAddressDTO() {
		return addressDTO;
	}
	public void setAddressDTO(AddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}
	@Valid
	private AddressDTO addressDTO;
    	
}
