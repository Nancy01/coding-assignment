package com.rabo.customer.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EntryExitLoggerConfig {
	 private static final Logger logger = LogManager.getLogger(EntryExitLoggerConfig.class);
	 
	    @Around("@annotation(com.rabo.customer.annotation.EntryExitLogger)")
	    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable{
	        Object response = null;
	        String method = joinPoint.getSignature().toShortString();
	        try {
	            logger.info("Entering into - " + method);
	            response = joinPoint.proceed();
	            logger.info("Exiting from - " + method);
	            return response;
	        } catch (Exception e) {
	            logger.error("Exception while invoking method - " + method);
	            throw e;
	        }
	    }
}
