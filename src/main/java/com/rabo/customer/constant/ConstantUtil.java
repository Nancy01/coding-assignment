package com.rabo.customer.constant;

public class ConstantUtil {

	public static final int  DATABASE_ERROR_STATUS_CODE = 602;
	public static final String ERROR_RETRIEVING_CUSTOMER = "An error occurred while retrieving customers.";
	public static final String ERROR_ADDING_CUSTOMER = "An error occurred while adding customer.";
	public static final String CUSTOMER_NOT_FOUND = "Customer does not exist.";
	public static final String ERROR_UPDATING_ADDRESS = "An error occurred while updating address.";
	public static final String CUSTOMER_ADDED_SUCCESSFULLY = "Customer added successfully";
	public static final String CUSTOMER_FOUND_SUCCESSFULLY = "Customer found successfully with customer id : ";
}
