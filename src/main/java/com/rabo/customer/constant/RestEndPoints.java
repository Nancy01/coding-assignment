package com.rabo.customer.constant;


/**
 * 
 * Maintains all the REST end point url
 *
 */
public class RestEndPoints {

	public static final String API = "/api";
	public static final String CUSTOMER_ID = "/customer/{id}";
	public static final String CUSTOMER = "/customer";
	public static final String SEARCH_CUSTOMER = "/customer/search";
}
