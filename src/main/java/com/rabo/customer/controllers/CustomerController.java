package com.rabo.customer.controllers;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rabo.customer.DTO.CustomerDTO;
import com.rabo.customer.annotation.EntryExitLogger;
import com.rabo.customer.constant.ConstantUtil;
import com.rabo.customer.constant.RestEndPoints;
import com.rabo.customer.exceptions.BaseExceptionHandler;
import com.rabo.customer.form.CustomerForm;
import com.rabo.customer.models.Customer;
import com.rabo.customer.service.CustomerService;

/**
 * Handles all the incoming requests for Customer
 */
@RestController
@RequestMapping(RestEndPoints.API)
public class CustomerController extends BaseExceptionHandler {

	private static final Logger log = LogManager.getLogger(CustomerController.class);

	@Autowired
	private CustomerService customerService;

	/**
	 * Get all customers
	 * 
	 * @return
	 */
	@GetMapping(RestEndPoints.CUSTOMER)
	@EntryExitLogger
	public ResponseEntity<Object> getAllCustomers() {
		List<Customer> customers = customerService.getAllCustomers();
		if (customers.isEmpty()) {
			log.debug(ConstantUtil.CUSTOMER_NOT_FOUND);
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(customers.stream().map(customerService::convertToDto).collect(Collectors.toList()));
	}

	/**
	 * Gets customer by id.
	 *
	 * @param customerId customer id
	 * @return customer by id
	 */
	@GetMapping(RestEndPoints.CUSTOMER_ID)
	@EntryExitLogger
	public ResponseEntity<Object> getCustomerById(@PathVariable(value = "customerId") String customerId) {
		Customer customer = null;
		CustomerDTO customerDTO = null;
		try {
			customer = customerService.getCustomerById(customerId);
			if (Objects.nonNull(customer)) {
				log.debug(ConstantUtil.CUSTOMER_FOUND_SUCCESSFULLY + customerId);
				customerDTO = customerService.convertToDto(customer);
			} else {
				log.debug(ConstantUtil.CUSTOMER_NOT_FOUND);
				return ResponseEntity.notFound().build();
			}
		} catch (Exception ex) {
			log.error(ConstantUtil.ERROR_RETRIEVING_CUSTOMER, ex);
			return handle500ServerError(ConstantUtil.ERROR_RETRIEVING_CUSTOMER);
		}
		return ResponseEntity.ok(customerDTO);
	}

	/**
	 * Adds a customer
	 *
	 * @param customer
	 * @return customer
	 */
	@PostMapping(RestEndPoints.CUSTOMER)
	@EntryExitLogger
	public ResponseEntity<Object> addCustomer(@Valid @RequestBody CustomerForm customerForm) {
		Customer persistentCustomer = null;
		try {
			persistentCustomer = customerService.saveCustomer(customerForm);
			log.debug(ConstantUtil.CUSTOMER_ADDED_SUCCESSFULLY + " : " + persistentCustomer.getId());
		} catch (PersistenceException pe) {
			log.error(ConstantUtil.ERROR_ADDING_CUSTOMER, pe);
			return handle602DatabaseServerError(ConstantUtil.ERROR_ADDING_CUSTOMER);
		} catch (Exception ex) {
			log.error(ConstantUtil.ERROR_ADDING_CUSTOMER, ex);
			return handle500ServerError(ConstantUtil.ERROR_ADDING_CUSTOMER);
		}
		return ResponseEntity.ok(customerService.convertToDto(persistentCustomer));
	}

	/**
	 * Searches a Customer for below criteria<br>
	 * by first name and last Name<br>
	 * by first name or by last name
	 * 
	 * @param firstName first name
	 * @return lastName last name
	 * @throws CustomerNotFoundException customer not found exception
	 */
	@GetMapping(RestEndPoints.SEARCH_CUSTOMER)
	@EntryExitLogger
	public ResponseEntity<Object> searchCustomer(@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName) {
		List<Customer> customerList = null;
		try {
			customerList = customerService.searchCustomer(firstName, lastName);
			if (customerList.isEmpty()) {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception ex) {
			log.error(ConstantUtil.ERROR_RETRIEVING_CUSTOMER, ex);
			return handle500ServerError(ConstantUtil.ERROR_RETRIEVING_CUSTOMER);
		}
		return ResponseEntity.ok(customerList.stream().map(customerService::convertToDto).collect(Collectors.toList()));
	}

	/**
	 * Updates an address of a customer
	 *
	 * @param id      customer id
	 * @param address address
	 * @return customer
	 */
	@PutMapping(RestEndPoints.CUSTOMER_ID)
	@EntryExitLogger
	public ResponseEntity<Object> updateAddress(@Valid @RequestBody CustomerDTO customerDTO) {
		Customer persistentCustomer = null;
		try {
			persistentCustomer = customerService.getCustomerById(customerDTO.getCustomerId());
			if (Objects.isNull(persistentCustomer)) {
				return ResponseEntity.notFound().build();
			} else {
				synchronized (persistentCustomer) {
					customerService.updateAddress(persistentCustomer, customerDTO);
				}
			}
		} catch (PersistenceException pe) {
			log.error(ConstantUtil.ERROR_UPDATING_ADDRESS, pe);
			return handle602DatabaseServerError(ConstantUtil.ERROR_UPDATING_ADDRESS);
		} catch (Exception ex) {
			log.error(ConstantUtil.ERROR_UPDATING_ADDRESS, ex);
			return handle500ServerError(ConstantUtil.ERROR_UPDATING_ADDRESS);
		}
		return ResponseEntity.ok(customerService.convertToDto(persistentCustomer));
	}

}
