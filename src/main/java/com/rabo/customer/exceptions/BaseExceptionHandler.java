package com.rabo.customer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.rabo.customer.constant.ConstantUtil;

/**
 * 
 * This is the base exception class for exception handelling<br>
 * and returns the HTTP response along with status code to the client 
 */
public abstract class BaseExceptionHandler {

	
	/**Returns HTTP-Code 400<br>
	 * if the request is malformed
	 * @param message
	 * @return
	 */
	protected ResponseEntity<Object> handle400BadRequest(Object message)
	{
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
	}
	
	/**Returns HTTP-Code 500<br>
	 * if any internal server error occurs
	 * @param message
	 * @return
	 */
	protected ResponseEntity<Object> handle500ServerError(Object message){
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
	}
	
	/**Returns custom HTTP-Code 602<br>
	 * if any database exception occurs
	 * @param message
	 * @return
	 */
	protected ResponseEntity<Object> handle602DatabaseServerError(Object message){
		return ResponseEntity.status(ConstantUtil.DATABASE_ERROR_STATUS_CODE).body(message);
	}
}
