package com.rabo.customer.form;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.rabo.customer.common.BaseAddress;

/**
 * 
 * Maps user's inputs
 *
 */
public class CustomerForm {
	
	private String firstName;
	@NotBlank(message = "Last Name must not be blank.")
	private String lastName;
	@NotNull(message = "Age must not be null.")
	private LocalDate age;
    @Valid
	private BaseAddress baseAddress;

	public BaseAddress getBaseAddress() {
		return baseAddress;
	}
	public void setBaseAddress(BaseAddress baseAddress) {
		this.baseAddress = baseAddress;
	}
    
	public LocalDate getAge() {
		return age;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

}
