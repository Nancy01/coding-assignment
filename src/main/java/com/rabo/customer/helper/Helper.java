package com.rabo.customer.helper;

import java.time.LocalDate;
import java.time.Period;

public class Helper {

	private static long counter = 100;

	/**
	 * Helper method to generate custom ID for customer
	 * @return
	 */
	public static synchronized String generateCustomerId()
	{
	    return String.valueOf(counter++);
	} 
	
	/**
	 * Helper method to calculate age
	 * @param birthday
	 * @return
	 */
	public int calculateAge(LocalDate birthday)
	{
		LocalDate today = LocalDate.now();  //Today's date
		Period p =  Period.between(birthday, today);
		return p.getYears();
		 
	}
	
}
