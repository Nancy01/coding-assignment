package com.rabo.customer.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rabo.customer.models.Customer;

/**
 * 
 * Handles CRUD operations on the Customer entity
 *
 */
@Repository("customerRepository")
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	List<Customer>findByFirstNameOrLastNameOrFirstNameAndLastName(String orFirstName, String orLastName, String firstName, String lastName);
	
	Customer findByCustomerId(String customerId);
	/*@Query("from Customer where id=:id")
	Customer findCustomerById(@Param("id") Long id);*/
}
