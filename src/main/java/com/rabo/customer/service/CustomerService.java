package com.rabo.customer.service;

import java.util.List;

import javax.validation.Valid;

import com.rabo.customer.DTO.CustomerDTO;
import com.rabo.customer.form.CustomerForm;
import com.rabo.customer.models.Address;
import com.rabo.customer.models.Customer;

public interface CustomerService {

	Customer saveCustomer(@Valid CustomerForm customerForm);

	void saveAddress(Address persistentAddress, Address address);

	CustomerDTO convertToDto(Customer persistentCustomer);

	List<Customer> getAllCustomers();

	Customer getCustomerById(String customerId);

	List<Customer> searchCustomer(String firstName, String lastName);

	void updateAddress(Customer persistentCustomer, @Valid CustomerDTO customerDTO);

}
