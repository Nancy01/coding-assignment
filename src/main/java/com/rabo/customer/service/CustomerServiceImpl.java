package com.rabo.customer.service;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rabo.customer.DTO.AddressDTO;
import com.rabo.customer.DTO.CustomerDTO;
import com.rabo.customer.form.CustomerForm;
import com.rabo.customer.helper.Helper;
import com.rabo.customer.models.Address;
import com.rabo.customer.models.Customer;
import com.rabo.customer.repositories.CustomerRepository;

/**
 * 
 * Customer Service
 *
 */
@Service("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CustomerRepository customerRepository;

	/**
	 * Maps the value of DTO to persistent Customer object
	 */
	@Override
	public Customer saveCustomer(@Valid CustomerForm customerForm) {
		Customer customer = modelMapper.map(customerForm, Customer.class);
		customer.setCustomerId(Helper.generateCustomerId());
		Address address = modelMapper.map(customerForm.getBaseAddress(), Address.class);
		customer.setAddress(address);
		return customerRepository.save(customer);
	}

	@Override
	public void saveAddress(Address persistentAddress, Address address) {
		persistentAddress.setStreet1(address.getStreet1());
		persistentAddress.setStreet2(address.getStreet2());
		persistentAddress.setCity(address.getCity());
		persistentAddress.setCountry(address.getCountry());
		persistentAddress.setZipCode(address.getZipCode());
	}

	/**
	 * Converts the persistent object to DTO
	 */
	@Override
	public CustomerDTO convertToDto(Customer customer) {
		AddressDTO addressDTO = modelMapper.map(customer.getAddress(), AddressDTO.class);
		CustomerDTO customerDTO = modelMapper.map(customer, CustomerDTO.class);
		customerDTO.setAddressDTO(addressDTO);
		return customerDTO;
	}

	/**
	 * Retrieves all customers
	 */
	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	/**
	 * Retrieves a customer by Id
	 */
	@Override
	public Customer getCustomerById(String customerId) {
		return customerRepository.findByCustomerId(customerId);
	}

	/**
	 * Search customer
	 */
	@Override
	public List<Customer> searchCustomer(String firstName, String lastName) {
		return customerRepository.findByFirstNameOrLastNameOrFirstNameAndLastName(firstName, lastName, firstName,
				lastName);
	}

	/**
	 * Updates customer's address
	 */
	@Override
	public void updateAddress(Customer persistentCustomer, @Valid CustomerDTO customerDTO) {
		Address address = modelMapper.map(customerDTO.getAddressDTO(), Address.class);
		persistentCustomer.setAddress(address);
	}

}
